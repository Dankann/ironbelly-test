﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public sealed class FindNearestNeighbour : MonoBehaviour
{
    public List<FindNearestNeighbour> AvailableNeighbours => FindNearestNeighbourManager.Spawned;

    public FindNearestNeighbourManager FindNearestNeighbourManager
    {
        get
        {
            if (_findNearestNeighbourManager == null)
            {
                _findNearestNeighbourManager = FindObjectOfType<FindNearestNeighbourManager>();
            }
            return _findNearestNeighbourManager;
        }
    }

    public LineRenderer LineRenderer
    {
        get
        {
            if (_lineRenderer == null)
            {
                _lineRenderer = GetComponent<LineRenderer>();
            }
            return _lineRenderer;
        }
    }

    [SerializeField] private FindNearestNeighbourPool _findNearestNeighbourPool;
    [SerializeField] private FindNearestNeighbour _nearestNeighbour;

    private FindNearestNeighbourManager _findNearestNeighbourManager;
    private LineRenderer _lineRenderer;

    private void FixedUpdate()
    {
        Vector3 originPosition = transform.position;
        Vector3 nearestNeighbourPosition = Vector3.zero;
        float nearestNeighbourDistance = float.MaxValue;
        bool foundNearestNeighbour = false;

        for (int i = 0; i < AvailableNeighbours.Count; i++)
        {
            if (AvailableNeighbours[i] == this)
            {
                continue;
            }

            Vector3 neighbourPosition = AvailableNeighbours[i].transform.position;
            Vector3 distance = neighbourPosition - originPosition;
            float distanceSquared = (distance.x * distance.x) + (distance.y * distance.y) + (distance.z * distance.z);

            if (distanceSquared < nearestNeighbourDistance)
            {
                nearestNeighbourDistance = distanceSquared;
                nearestNeighbourPosition = neighbourPosition;
                foundNearestNeighbour = true;
            }
        }

        if (foundNearestNeighbour)
        {
            LineRenderer.positionCount = 2;
            LineRenderer.SetPositions(new Vector3[] { originPosition, nearestNeighbourPosition });
        }
        else
        {
            LineRenderer.positionCount = 0;
        }
    }

}
