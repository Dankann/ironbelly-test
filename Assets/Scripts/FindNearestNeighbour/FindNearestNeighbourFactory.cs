﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFindNearestNeighbourFactory", menuName = "Pool/FindNearestNeighbour Factory")]
public class FindNearestNeighbourFactory : Factory<FindNearestNeighbour>
{
    public FindNearestNeighbour prefab = default;

    public override FindNearestNeighbour Create()
    {
        return Instantiate(prefab);
    }
}
