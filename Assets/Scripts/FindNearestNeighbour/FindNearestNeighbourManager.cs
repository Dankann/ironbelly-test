﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public sealed class FindNearestNeighbourManager : MonoBehaviour
{
    public List<FindNearestNeighbour> Spawned => _spawned;

    public Zone Zone
    {
        get
        {
            if (_zone == null)
            {
                _zone = GetComponent<Zone>();
            }
            return _zone;
        }
    }

    [SerializeField] private FindNearestNeighbourPool _findNearestNeighbourPool;
    [SerializeField] private int _preWarmAmount;
    [SerializeField] private TMPro.TMP_InputField _inputField;
    [SerializeField] private TMPro.TextMeshProUGUI _textSpawnedCount;
    [SerializeField] private KeyCode _spawnKeyCode;
    [SerializeField] private KeyCode _despawnKeyCode;

    private List<FindNearestNeighbour> _spawned = new List<FindNearestNeighbour>();
    private Zone _zone;

    public void Awake()
    {
        Spawn(_preWarmAmount);
    }

    public void Update()
    {
        if (Input.GetKeyDown(_spawnKeyCode))
        {
            Spawn(Convert.ToInt32(_inputField.text));
        }

        if (Input.GetKeyDown(_despawnKeyCode))
        {
            Despawn(Convert.ToInt32(_inputField.text));
        }
    }

    public void Spawn(int amount)
    {
        var x = _findNearestNeighbourPool.Request(amount);
        foreach (var item in x)
        {
            item.transform.position = Zone.GetRandomPosition();
            var randomMovement = item.GetComponent<RandomMovementInZone>();
            if (randomMovement != null)
            {
                randomMovement.Zone = Zone;
            }
        }
        _spawned.AddRange(x);

        UpdateUI();
    }

    public void Despawn(int amount)
    {
        int index = Mathf.Clamp(_spawned.Count - (int)amount, 0, _spawned.Count - 1);
        List<FindNearestNeighbour> items = _spawned.GetRange(index, amount);
        _findNearestNeighbourPool.Return(items);
        _spawned.RemoveRange(index, amount);

        UpdateUI();
    }

    private void UpdateUI()
    {
        _textSpawnedCount.text = _spawned.Count().ToString();
    }
}

