﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFindNearestNeighbourPool", menuName = "Pool/FindNearestNeighbour Pool")]
public sealed class FindNearestNeighbourPool : PoolComponent<FindNearestNeighbour>
{
    [SerializeField]
    private FindNearestNeighbourFactory _factory;

    public override IFactory<FindNearestNeighbour> Factory
    {
        get
        {
            return _factory;
        }
        set
        {
            _factory = value as FindNearestNeighbourFactory;
        }
    }
}
