using System.Collections.Generic;
using UnityEngine;

public abstract class Pool<T> : ScriptableObject
{
    protected Stack<T> Available = new Stack<T>();

    public abstract IFactory<T> Factory { get; set; }
    protected bool WasPrewarmed;

    protected virtual T Create()
    {
        return Factory.Create();
    }

    public virtual void Prewarm(int amount)
    {
        if (WasPrewarmed)
        {
            return;
        }

        for (int i = 0; i < amount; i++)
        {
            Available.Push(Create());
        }

        WasPrewarmed = true;
    }

    public virtual T Request()
    {
        var item = Available.Count > 0 ? Available.Pop() : Create();

        return item;
    }

    public virtual IEnumerable<T> Request(int count = 1)
    {
        List<T> items = new List<T>(count);
        for (int i = 0; i < count; i++)
        {
            items.Add(Request());
        }
        return items;
    }

    public virtual void Return(T item)
    {
        Available.Push(item);
    }

    public virtual void Return(IEnumerable<T> items)
    {
        foreach (var item in items)
        {
            Return(item);
        }
    }
}
