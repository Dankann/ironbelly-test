﻿using UnityEngine;

public abstract class PoolComponent<T> : Pool<T> where T : Component
{
    private Transform _root;
    private Transform Root
    {
        get
        {
            if (_root == null)
            {
                _root = new GameObject(name).transform;
            }
            return _root;
        }
    }

    public override T Request()
    {
        T member = base.Request();
        member.gameObject.SetActive(true);
        return member;
    }

    public override void Return(T member)
    {
        member.transform.SetParent(Root.transform);
        member.gameObject.SetActive(false);
        base.Return(member);
    }

    protected override T Create()
    {
        T newItem = base.Create();
        newItem.transform.SetParent(Root.transform);
        newItem.gameObject.SetActive(false);
        return newItem;
    }
}
