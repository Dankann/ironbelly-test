﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Utils
{
    public class Lerp
    {
        public bool IsLerping;

        private float _lerpTime;

        public Lerp(float lerpTime)
        {
            this._lerpTime = lerpTime;
        }

        public async Task DoLerp(Action<float> onProgress, Action onFinish = null, CancellationToken token = default)
        {
            try
            {
                IsLerping = true;
                var currentTime = 0f;
                while (currentTime < _lerpTime && !token.IsCancellationRequested)
                {
                    currentTime += Time.deltaTime;
                    var perc = currentTime / _lerpTime;
                    onProgress?.Invoke(perc);
                    await Task.Yield();
                }
            }
            finally
            {
                IsLerping = false;
                onFinish?.Invoke();
            }
        }
    }
}