﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
public static class TaskExtensions
{
    public static async Task WaitUntilAsync(Func<bool> predicate, CancellationToken token = default)
    {
        try
        {
            while (!predicate() && !token.IsCancellationRequested)
                await Task.Yield();
        }
        catch (NullReferenceException)
        {
            throw;
        }
    }

}
