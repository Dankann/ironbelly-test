using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Utils;

public class RandomMovementInZone : MonoBehaviour
{
    public Zone Zone;

    [SerializeField] private float _velocity;

    private Lerp _lerp;
    private Vector3 _initialPosition;
    private Vector3 _destinationPosition;
    private CancellationTokenSource _cancellationTokenSource;

    private void OnEnable()
    {
        StartNewMovement();
    }

    private void OnDisable()
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(_destinationPosition, .2f);
    }
#endif

    public async void StartNewMovement()
    {
        await TaskExtensions.WaitUntilAsync(() => { return Zone != null; });

        if (_lerp != null && _lerp.IsLerping)
        {
            return;
        }

        _destinationPosition = Zone.GetRandomPosition();
        _initialPosition = transform.position;
        Vector3 distance = _initialPosition - _destinationPosition;
        float duration = distance.magnitude / _velocity;
        _lerp = new Lerp(duration);
        _cancellationTokenSource = new CancellationTokenSource();
        _ = _lerp.DoLerp(OnProgress, OnFinish, _cancellationTokenSource.Token);
    }

    private void OnProgress(float value)
    {
        transform.position = Vector3.Lerp(_initialPosition, _destinationPosition, value);
    }

    private void OnFinish()
    {
        if (!_cancellationTokenSource.IsCancellationRequested)
            StartNewMovement();
    }
}
