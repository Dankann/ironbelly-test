using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    //public Bounds Bounds => _bounds;

    [SerializeField] private Vector3 _zone;
    [SerializeField] private Vector3 Min => _center - _zone;
    [SerializeField] private Vector3 Max => _center + _zone;

    private Vector3 _center;

    //private Bounds _bounds;
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(_center, _zone * 2);
    }
#endif

    private void Awake()
    {
        UpdateBounds();
    }

    private void FixedUpdate()
    {
        UpdateBounds();
    }

    private void OnValidate()
    {
        UpdateBounds();
    }

    public Vector3 GetRandomPosition()
    {
        float x = Random.Range(Min.x, Max.x);
        float y = Random.Range(Min.y, Max.y);
        float z = Random.Range(Min.z, Max.z);

        return new Vector3(x, y, z);
    }

    private void UpdateBounds()
    {
        _center = transform.position;
    }
}
